import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import { StyleSheet, Text, View, TextInput, Button } from "react-native";

export default function App() {
  const [matrixSize, setMatrixSize] = useState("0");

  let data = [
    [1, 2, 3, 4, 5],
    [6, 7, 8, 9, 10],
    [11, 12, 13, 14, 15],
    [16, 17, 18, 19, 20],
    [21, 22, 23, 24, 25],
  ];

  const fullFillArray = (height, width) => {
    let array = [];

    let lastInteger = 0;

    console.log("----");
    for (let i = 0; i < height; i++) {
      console.log("-----");
      const size = lastInteger + width;

      for (let j = lastInteger + 1; j < size + 1; j++) {
        lastInteger = j;
        console.log(lastInteger);
      }
    }
  };

  const calculateMatrix = (size) => {
    let sizeInt = parseInt(size);

    let i;
    let startingRow = 0;
    let startingColumn = 0;
    let count = 0;

    let endingRow = sizeInt;
    let endingColumn = sizeInt;

    // Maximum items in Matrix
    let total = sizeInt * sizeInt;

    while (startingRow < endingRow && startingColumn < endingColumn) {
      if (count == total) break;

      for (i = startingRow; i < endingRow; ++i) {
        console.log(data[startingColumn][i]);
        count++;
      }
      startingColumn++;

      if (count == total) break;

      for (i = startingColumn; i < endingColumn; ++i) {
        console.log(data[i][endingRow - 1]);
        count++;
      }
      endingRow--;

      if (count == total) break;

      if (startingRow < endingRow) {
        for (i = endingRow - 1; i >= startingRow; --i) {
          console.log(data[endingColumn - 1][i]);
          count++;
        }
        endingColumn--;
      }

      if (count == total) break;

      if (startingColumn < endingColumn) {
        for (i = endingColumn - 1; i >= startingColumn; --i) {
          console.log(data[i][startingRow]);
          count++;
        }
        startingRow++;
      }
    }
  };

  return (
    <View style={styles.container}>
      <Text>Enter matrix width and height</Text>
      <TextInput
        style={styles.input}
        value={matrixSize}
        onChangeText={setMatrixSize}
      />
      <Button
        onPress={() => calculateMatrix(matrixSize, data)}
        title="Submit"
        color="#841584"
      />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});
