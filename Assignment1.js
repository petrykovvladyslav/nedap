import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import { Button, StyleSheet, Text, View } from "react-native";

export default function App() {
  const [currentScore, setCurrentScore] = useState(0);

  const finalBoardScore = 25;

  const snakeHeadCheck = (position) => {
    let positionToReturn = position;

    switch (position) {
      case 14:
        positionToReturn = 4;
        break;
      case 19:
        positionToReturn = 8;
        break;
      case 22:
        positionToReturn = 16;
        break;
      case 24:
        positionToReturn = 20;
        break;

      default:
        positionToReturn = position;
    }

    return positionToReturn;
  };

  const ladderCheck = (position) => {
    let positionToReturn = position;

    if(position === 3){
      positionToReturn = 11;  
    }else if(position === 9){
      positionToReturn = 18;  
    }else if(position === 10){
      positionToReturn = 12;  
    }else if(position === 6){
      positionToReturn = 17;  
    }else{
      positionToReturn = position;
    }

    return positionToReturn;
  };

  const rollDice = () => {
    const rolledDice = Math.floor(Math.random() * 6) + 1;
    const positionAfterDiceRoll = currentScore + rolledDice;
    
    const scoreAfterSnakeCheck = snakeHeadCheck(positionAfterDiceRoll);
    const scoreAfterLadderCheck = ladderCheck(scoreAfterSnakeCheck);
    setCurrentScore(scoreAfterLadderCheck);
  };

  return (
    <View style={styles.container}>
      {currentScore >= finalBoardScore && <Text>You won!</Text>}
      <Text>Current score:{currentScore}</Text>
      <Button onPress={rollDice} title="Roll a dice" color="#841584" />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
import { StatusBar } from "expo-status-bar";
import { useState } from "react";
import { Button, StyleSheet, Text, View } from "react-native";

export default function App() {
  const [currentScore, setCurrentScore] = useState(0);

  const finalBoardScore = 25;

  const snakeHeadCheck = (position) => {
    let positionToReturn = position;

    switch (position) {
      case 14:
        positionToReturn = 4;
        break;
      case 19:
        positionToReturn = 8;
        break;
      case 22:
        positionToReturn = 16;
        break;
      case 24:
        positionToReturn = 20;
        break;

      default:
        positionToReturn = position;
    }

    return positionToReturn;
  };

  const ladderCheck = (position) => {
    let positionToReturn = position;

    if(position === 3){
      positionToReturn = 11;  
    }else if(position === 9){
      positionToReturn = 18;  
    }else if(position === 10){
      positionToReturn = 12;  
    }else if(position === 6){
      positionToReturn = 17;  
    }else{
      positionToReturn = position;
    }

    return positionToReturn;
  };

  const rollDice = () => {
    const rolledDice = Math.floor(Math.random() * 6) + 1;
    const positionAfterDiceRoll = currentScore + rolledDice;
    
    const scoreAfterSnakeCheck = snakeHeadCheck(positionAfterDiceRoll);
    const scoreAfterLadderCheck = ladderCheck(scoreAfterSnakeCheck);
    setCurrentScore(scoreAfterLadderCheck);
  };

  return (
    <View style={styles.container}>
      {currentScore >= finalBoardScore && <Text>You won!</Text>}
      <Text>Current score:{currentScore}</Text>
      <Button onPress={rollDice} title="Roll a dice" color="#841584" />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
